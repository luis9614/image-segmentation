from Segmentation import Segmenter


image = "flower.bmp"
test = Segmenter(image_name=image, hsl=False, verbose=True, graph=True, save=True)
test.show_training_pixels()
#test.show_all_images()
error = test.compare_results()
print("Error RGB: ", error)

"""test = Segmenter(image_name=image, hsl=True, graph=True, save=True)
test.show_training_pixels()
#test.show_all_images()
error = test.compare_results()
print("Error HSL: ", error)"""
