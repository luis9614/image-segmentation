import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

def graph_hsl_vectors(arr, w, h):
    fig = plt.figure()
    vecs = fig.gca(projection='3d')
    for i in range(w):
        for j in range(h):
            x = arr[i][j][0]
            y = arr[i][j][1]
            z = arr[i][j][2]

        vecs.quiver(x, y, z, 0, 0, 0)
    vecs.set_xlim([0, 1.0])
    vecs.set_ylim([0, 1.0])
    vecs.set_zlim([0, 1.0])
    plt.show()