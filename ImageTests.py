from PIL import Image
import numpy as np
from os import listdir
from queue import Queue


def bfs(image):
    print(image.shape)
    w, h = image.shape
    print("BFS")
    # Checks for white pixels
    used = np.zeros((w, h),dtype=int)
    for i in range(w):
        for j in range(h):
            if image[i][j] == 255 and used[i][j]==0:
                print("White pixel at: ", i, ", ", j)
                island = get_island(i, j, image, w, h, used)
                print(len(island)/(w*h))
                if len(island)/(w*h) <= 0.03:
                    for i in range(len(island)):
                        i, j = island[i]
                        image[i][j] = 0


def get_island(i, j, image, w, h, used):
    queue = Queue()
    queue.put((i,j))
    island = []
    used[i, j] = 1
    while not queue.empty():
        x, y = queue.get()
        if used[(x+1)%w, y] == 0 and image[(x+1)%w,y] !=0:
            queue.put(((x+1)%w, y))
            used[(x+1)%w, y] = 1
        if used[(x-1)%w, y] == 0 and image[(x-1)%w,y] !=0:
            queue.put(((x-1)%w, y))
            used[(x-1)%w, y] = 1

        if used[x, (y+1)%h] == 0 and image[x,(y+1)%h] !=0:
            queue.put((x, (y+1)%h))
            used[x, (y+1)%h] = 1
        if used[x, (y-1)%h] == 0 and image[x,(y-1)%h] !=0:
            queue.put((x, (y-1)%h))
            used[x, (y-1)%h] = 1

        island.append((x, y))
    return island



image = Image.open("images/luis_borders/flower.bmp")
image = image.convert("L")
image.show()
image = np.array(image)
bfs(image)
img = Image.fromarray(image)
img.show()

