from Segmentation import Segmenter
from os import listdir

file_names = listdir("images/images/")
error = 0.0
errorHSL = 0.0
cont = 1

for file in file_names:
    if file != ".DS_Store":
        print(cont, ".-Working on ", file)
        test = Segmenter(file, hsl=False, graph=False, save=True)

        error += test.compare_results()
        print("Error Acumulado: ", error, "Error %: ", error / cont)
        #testHSL = Segmenter(file, hsl=True, graph=False, save=True)
        #errorHSL += testHSL.compare_results()
        #print("Error Acumulado HSL: ", errorHSL, "ErrorHSL %", errorHSL/cont)
        print("\n")
        cont += 1

error = error/(len(file_names)-1)
errorHSL = errorHSL/(len(file_names)-1)
print("\nError RGB: ", error*100)
