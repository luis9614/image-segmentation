import numpy as np
import colorsys
from GraphingUtilities import graph_hsl_vectors


def rgb_to_hsl(x, w, h, c, graph):
    print("Converting Image to HSL")
    y = np.zeros(x.shape)
    for i in range(w):
        for j in range(h):
            aux_pixel = x[i][j]/255
            hsl_color = colorsys.rgb_to_hls(aux_pixel[0], aux_pixel[1], aux_pixel[2])
            y[i][j] = np.array(hsl_color)
    if graph:
        print(y)
        graph_hsl_vectors(y, w, h)
    print("\tDone")
    return y

def hsl_to_rgb():
    pass