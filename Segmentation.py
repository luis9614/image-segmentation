import numpy as np
from PIL import Image
from ImageProcessingUtilities import rgb_to_hsl
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.neighbors import KNeighborsClassifier
from scipy import ndimage
import math
from queue import Queue


class Segmenter:
    def __init__(self, image_name="image", hsl=False, graph=False, verbose=False, save=False):
        self.name = image_name
        self.hsl = hsl
        self.verbose = verbose
        self.graph = graph
        self.save = save
        self.name = image_name
        self.original_image = np.array(Image.open("images/images/"+image_name))
        self.w, self.h, self.c = self.original_image.shape
        if self.hsl:
            self.original_image = rgb_to_hsl(self.original_image, self.w, self.h, self.c, graph) * 255
        else:
            self.original_image = self.original_image
            #print("ELSE RGB:", self.original_image)
        self.marked_image = np.array(Image.open("images/bordes_mr/"+image_name))
        self.result_image = np.zeros((self.w, self.h), dtype=int) # Si no sirve el algoritmo sacará una imagen en negro
        self.start_processing()

        if graph:
            #self.compare_results()
            pass

    def start_processing(self):
        print("Processing data")
        self.x_subject = self.original_image[self.marked_image==255]
        self.x_background = self.original_image[self.marked_image == 64]

        #   Añadir la información de la posición del pixel como variables de entrada
        img_s = self.marked_image==255
        x_list = []
        y_list = []
        for i in range(self.w):
            for j in range(self.h):
                if img_s[i][j]:
                    x_list.append(i)
                    y_list.append(j)
        x_list = np.expand_dims(np.array(x_list),axis=0)
        y_list = np.expand_dims(np.array(y_list), axis=0)
        self.x_subject = np.concatenate((self.x_subject, x_list.T), axis=1)
        self.x_subject = np.concatenate((self.x_subject, y_list.T), axis=1)

        img_b = self.marked_image == 64
        x_list = []
        y_list = []
        for i in range(self.w):
            for j in range(self.h):
                if img_b[i][j]:
                    x_list.append(i)
                    y_list.append(j)

        x_list = np.expand_dims(np.array(x_list), axis=0)
        y_list = np.expand_dims(np.array(y_list), axis=0)
        self.x_background = np.concatenate((self.x_background, x_list.T), axis=1)
        self.x_background = np.concatenate((self.x_background, y_list.T), axis=1)

        # Este es el intento de pasar la entrada a tener 5 variables
        #   Hay que modificar X para que tenga las 5 componentes de cada pixel,
        indx_array = []
        for i in range(self.w):
            for j in range(self.h):
                 indx_array.append([i, j])

        indx_array = np.array(indx_array)

        X = self.original_image.reshape((self.w * self.h), self.c)
        # print(X.shape)
        X = np.concatenate((X, indx_array), axis=1)

        x_train = np.append(self.x_subject, self.x_background, axis=0)
        labels = np.append((np.ones(len(self.x_subject), float) * 255), (np.ones(len(self.x_background), float) * 0),
                           axis=0)

        print("\tDone")

        print("Creating ensemble")
        SVMModel = SVC(kernel="linear", max_iter=200000)
        SVMModel2 = SVC(kernel="poly", max_iter=100000)
        KNNModel = KNeighborsClassifier(n_neighbors=5)
        LRModel = LogisticRegression()
        DTModel = DecisionTreeClassifier()
        ensemble = VotingClassifier(estimators=[("svm", SVMModel), ("knn", KNNModel), ("lr", LRModel), ("dt", DTModel), ("svm2", SVMModel2)], voting='hard')
        print("\tDone")
        print("Fitting Ensemble")
        ensemble.fit(x_train, labels)
        print("\tDone")
        print("Predicting")
        res = ensemble.predict(X)
        print("\tDone")
        self.result_image = res.reshape((self.w, self.h))
        self.post_processing()

        if self.save:
            img = Image.fromarray(self.result_image).convert('RGB')
            if self.hsl:
                img.save("images/luis_borders/hsl_" + self.name)
            else:
                img.save("images/luis_borders/" + self.name)

    def compare_results(self):
        #   Opens real border image as numpy array
        img = Image.open("images/bordes_real/" + self.name)
        comp = np.array(img)
        cont = 0
        for i in range(self.w):
            for j in range(self.h):
                if comp[i][j] != self.result_image[i][j] and comp[i][j] != 128:
                    cont += 1
        total = (self.w*self.h)
        if self.graph:
            img.show()
            img = Image.fromarray(self.result_image.astype('uint8'))
            img.show()
        print("Error: ", cont, "/", total, " = ", cont/total*100 ) if self.verbose else None
        return cont/total

    def show_training_pixels(self):
        #   Shows the marked pixels, using the original
        #   image's colors.
        aux = np.zeros(self.original_image.shape)
        aux[self.marked_image == 255] = self.original_image[self.marked_image==255]
        aux[self.marked_image == 64] = self.original_image[self.marked_image == 64]
        img = Image.fromarray(aux.astype('uint8'))
        img.show(title="Pixeles de Entrenamiento")


    def show_all_images(self):
        #self.original_image = self.original_image * 255
        current_img = Image.fromarray(self.original_image.astype('uint8'))
        current_img.show(title=self.name + "_original_image")

        current_img = Image.fromarray(self.marked_image.astype('uint8'))
        current_img.show(title=self.name + "_marked_pixels")

        #   Should show result image
        current_img = Image.fromarray(self.result_image.astype('uint8'))
        current_img.show(title=self.name + "_result")

    #   Just for testing
    def gaussian_distance(self, x, l, sigma=1):
        return math.pow(np.dot(x - l, x - l), 2) / 2

    def post_processing(self):
        np.set_printoptions(threshold=np.inf)
        img = ndimage.binary_erosion(self.result_image).astype(self.result_image.dtype) * 255
        img = ndimage.binary_dilation(self.result_image).astype(self.result_image.dtype) * 255
        img = ndimage.binary_opening(self.result_image).astype(self.result_image.dtype) * 255
        self.bfs()
        self.result_image = img

    def bfs(self):
        print("BFS")
        # Checks for white pixels
        used = np.zeros((self.w, self.h), dtype=int)
        for i in range(self.w):
            for j in range(self.h):
                if self.result_image[i][j] == 255 and used[i][j] == 0:
                    #print("White pixel at: ", i, ", ", j)
                    island = self.get_island(i, j, used)
                    #print(len(island) / (self.w * self.h))
                    if len(island) / (self.w * self.h) <= 0.1:
                        for i in range(len(island)):
                            i, j = island[i]
                            self.result_image[i][j] = 0

    def get_island(self, i, j, used):
        queue = Queue()
        queue.put((i, j))
        island = []
        used[i, j] = 1
        while not queue.empty():
            x, y = queue.get()
            if used[(x + 1) % self.w, y] == 0 and self.result_image[(x + 1) % self.w, y] != 0:
                queue.put(((x + 1) % self.w, y))
                used[(x + 1) % self.w, y] = 1
            if used[(x - 1) % self.w, y] == 0 and self.result_image[(x - 1) % self.w, y] != 0:
                queue.put(((x - 1) % self.w, y))
                used[(x - 1) % self.w, y] = 1

            if used[x, (y + 1) % self.h] == 0 and self.result_image[x, (y + 1) % self.h] != 0:
                queue.put((x, (y + 1) % self.h))
                used[x, (y + 1) % self.h] = 1
            if used[x, (y - 1) % self.h] == 0 and self.result_image[x, (y - 1) % self.h] != 0:
                queue.put((x, (y - 1) % self.h))
                used[x, (y - 1) % self.h] = 1

            island.append((x, y))
        return island